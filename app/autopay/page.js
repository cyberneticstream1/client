"use client";
import Link from "next/link";
import React from "react";
import Image from "next/image"

import { styled } from '@mui/material/styles';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Switch from '@mui/material/Switch';
import Stack from '@mui/material/Stack';
import Typography from '@mui/material/Typography';

import PropTypes from 'prop-types';
import Slider, { SliderThumb } from '@mui/material/Slider';
import Tooltip from '@mui/material/Tooltip';
import Box from '@mui/material/Box';
import Content from "../../components/content";

export default function Page(){
    return (

            <>
            <Content children={
                <>

                {/* MOBILE AUTOPAY ACTIVE OR NOT TITLE WITH BACK BUTTON */}


                        <Link href = {"/"} className={"opacity-100"}>{"<--"}</Link> <br/>
                        <Link href = {"/"} className={"opacity-100"}>{"autopay: active"}</Link> <br/>


                        {/* MOBILE LOGIN BUTTONS */}
                     <div className={"mt-5 text-xl md:text-[45px]   opacity-75"}>
                    <button className={" p-3 md:p-9 mx-5 bg-purple-400 rounded-xl rounded-full "} onClick={() => alert(4)}> cyberneticstream@icloud.com </button>
                    <button className={" p-3  md:p-9 mx-5  mt-6 bg-purple-400 rounded-xl rounded-full"} onClick={() => alert(4)}> cyberneticstream@icloud.com </button>
                    <button className={" p-3  md:p-9 mx-5 mt-6 bg-purple-400 rounded-xl rounded-full"} onClick={() => alert(4)}> cyberneticstream@icloud.com </button>
                    <button className={"p-3 md:p-9 mx-5 mt-6 bg-purple-400 rounded-xl rounded-full "} onClick={() => alert(4)}> 510-388-5318 </button>
                     </div>

                        {/* MOBILE AUTOPAY LOGGED IN COMPONENT */}
                        <div className={"hidden"}>
                            <FormControlLabel
                                control={<IOSSwitch sx={{ m: 1 }} defaultChecked />}
                                label="autopay setting" size={"large"}
                            />
                        </div>


                </>
            }/>
            </>

    )
}

const IOSSwitch = styled((props) => (
        <Switch focusVisibleClassName=".Mui-focusVisible" disableRipple {...props} />
        ))(({ theme }) => ({
    width: 42,
    height: 26,
    padding: 0,
    '& .MuiSwitch-switchBase': {
        padding: 0,
        margin: 2,
        transitionDuration: '300ms',
        '&.Mui-checked': {
            transform: 'translateX(16px)',
            color: '#fff',
            '& + .MuiSwitch-track': {
                backgroundColor: theme.palette.mode === 'dark' ? '#2ECA45' : '#65C466',
                opacity: 1,
                border: 0,
            },
            '&.Mui-disabled + .MuiSwitch-track': {
                opacity: 0.5,
            },
        },
        '&.Mui-focusVisible .MuiSwitch-thumb': {
            color: '#33cf4d',
            border: '6px solid #fff',
        },
        '&.Mui-disabled .MuiSwitch-thumb': {
            color:
            theme.palette.mode === 'light'
            ? theme.palette.grey[100]
            : theme.palette.grey[600],
        },
        '&.Mui-disabled + .MuiSwitch-track': {
            opacity: theme.palette.mode === 'light' ? 0.7 : 0.3,
        },
    },
    '& .MuiSwitch-thumb': {
        boxSizing: 'border-box',
        width: 22,
        height: 22,
    },
    '& .MuiSwitch-track': {
        borderRadius: 26 / 2,
        backgroundColor: theme.palette.mode === 'light' ? '#E9E9EA' : '#39393D',
        opacity: 1,
        transition: theme.transitions.create(['background-color'], {
            duration: 500,
        }),
    },
}));